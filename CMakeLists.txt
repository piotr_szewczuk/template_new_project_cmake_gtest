#
# Created by Piotr Szewczuk on 2019-02-10.
#

cmake_minimum_required ( VERSION 3.12.0 )

## TODO change project name
project ( cmake_gtest VERSION 0.1 )


#############################
###   COMPILER SETTINGS   ###
#############################

set ( CMAKE_CXX_STANDARD 14 )
set ( CMAKE_CXX_STANDARD_REQUIRED ON )
set ( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread -pedantic -Wall -Wextra -Werror" )


###############################
###         TESTING        ####
###############################

enable_testing ()


###############################
###        BUILD DIR       ####
###############################

set ( CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR} )
set ( CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR} )
set ( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR} )


###############################
###   EXTERNAL LIBRARIES   ####
###############################

include_directories (
    dependencies/googletest/googletest/include
    dependencies/googletest/googlemock/include )

link_directories ( dependencies/googletest/build/lib )


###############################
###     PROJECT SUBDIRS    ####
###############################

add_subdirectory ( src )
add_subdirectory ( test )
